DROP DATABASE IF EXISTS employees;
CREATE DATABASE IF NOT EXISTS employees;
USE employees;

DROP TABLE IF EXISTS dept_emp,
                     dept_manager,
                     titles,
                     salaries, 
                     employees, 
                     departments;
					 
CREATE TABLE employees (
employee_number INT NOT NULL,
birth_date      DATE NOT NULL,
first_name      VARCHAR(30) NOT NULL,
last_name       VARCHAR(30) NOT NULL,
gender          ENUM ('M', 'F') NOT NULL,
PRIMARY KEY (employee_number)
);

CREATE TABLE departments (
department_numebr CHAR(10) NOT NULL,
department_name CHAR(100) NOT NULL,
employee_number INT NOT NULL,
PRIMARY KEY (department_numebr),
UNIQUE KEY (department_numebr, department_name),
FOREIGN KEY (employee_number) REFERENCES employees (employee_number)
);

CREATE TABLE departments_managers (
employee_number INT NOT NULL,
department_numebr CHAR(10) NOT NULL,
FOREIGN KEY (employee_number) REFERENCES employees (employee_number),
FOREIGN KEY (department_numebr) REFERENCES departments (department_numebr),
PRIMARY KEY (employee_number, department_numebr)
);

CREATE TABLE position (
employee_number INT NOT NULL,
position_name VARCHAR(80) NOT NULL,
hire_date DATE NOT NULL,
date_of_dismissal DATE,
FOREIGN KEY (employee_number) REFERENCES employees (employee_number) ON DELETE CASCADE,
PRIMARY KEY (employee_number, hire_date)
);

CREATE TABLE salaries (
employee_number INT NOT NULL,
salary INT NOT NULL,
FOREIGN KEY (employee_number) REFERENCES employees (employee_number) ON DELETE CASCADE,
PRIMARY KEY (employee_number)
);

CREATE INDEX indx_departments ON departments (department_numebr, department_name, employee_number);
					 